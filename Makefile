build: build/index.html build/main.js

build/index.html: public/index.html
	mkdir -p build/
	cp $< $@

build/main.js: src/Main.elm
	elm make $< --optimize --output="$@"
	uglifyjs "$@" \
		--compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" \
		| uglifyjs --mangle > build/main.js.uglified
	mv build/main.js.uglified "$@"
	bash bin/add-license $@

develop: develop/index.html develop/main.js

develop/main.js: src/Main.elm
	elm make $< --output=$@ --debug
	bash bin/add-license $@

develop/index.html: public/index.html
	mkdir -p develop/
	cp $< $@
