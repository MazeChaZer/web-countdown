port module Main exposing (main)

import Browser
import Browser.Events exposing (onAnimationFrame)
import Browser.Navigation exposing (Key)
import Dict
import Element
    exposing
        ( Color
        , Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , html
        , htmlAttribute
        , layout
        , none
        , padding
        , paragraph
        , px
        , rgb
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Font as Font
import Html
import Html.Attributes as Attributes
import Json.Decode as Decode exposing (Decoder, decodeString)
import Json.Decode.Pipeline exposing (required)
import Time exposing (Month(..), Posix, Zone)
import Time.Extra exposing (partsToPosix)
import TimeZone exposing (zones)
import Url exposing (Url, percentDecode)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChange
        , onUrlRequest = \_ -> NoOp
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { viewport : Viewport
    , prefersDarkColorScheme : Bool
    }


type alias Model =
    { state : State
    , viewport : Viewport
    , prefersColorScheme : ColorSchemePreference
    }


type alias Viewport =
    { width : Int
    , height : Int
    }


type ColorSchemePreference
    = Light
    | Dark


type State
    = FormPage
    | InitializingDisplay InitializingDisplayData
    | Display DisplayData
    | InvalidInput InvalidInputError


type InvalidInputError
    = MalformedUrl
    | JsonError Decode.Error


type alias InitializingDisplayData =
    { event : String
    , date : Posix
    , maybeLocalizedDateString : Maybe String
    , maybeNow : Maybe Posix
    , previousDisplay : Maybe DisplayData
    }


type alias DisplayData =
    { event : String
    , date : Posix
    , localizedDateString : String
    , now : Posix
    }


type alias Input =
    { event : String
    , year : Int
    , month : Month
    , day : Int
    , hour : Int
    , minute : Int
    , second : Int
    , timeZone : ( String, Zone )
    }


type alias ChunkedTimespan =
    { days : Int
    , hours : Int
    , minutes : Int
    , seconds : Int
    }


type alias LocalizedDateStringRequest =
    { milliseconds : Int
    , timeZone : String
    , includeSeconds : Bool
    }


type Msg
    = ReceiveLocalizedDateString String
    | ReceiveViewportSize Int Int
    | ReceivePrefersDarkColorScheme Bool
    | Tick Posix
    | UrlChange Url
    | NoOp


type alias ColorScheme =
    { background : Color
    , foreground : Color
    }


inputDecoder : Decoder Input
inputDecoder =
    Decode.succeed Input
        |> required "event" Decode.string
        |> required "year" Decode.int
        |> required "month" monthDecoder
        |> required "day" Decode.int
        |> required "hour" Decode.int
        |> required "minute" Decode.int
        |> required "second" Decode.int
        |> required "time-zone" zoneDecoder


monthDecoder : Decoder Month
monthDecoder =
    Decode.andThen
        (\x ->
            case x of
                1 ->
                    Decode.succeed Jan

                2 ->
                    Decode.succeed Feb

                3 ->
                    Decode.succeed Mar

                4 ->
                    Decode.succeed Apr

                5 ->
                    Decode.succeed May

                6 ->
                    Decode.succeed Jun

                7 ->
                    Decode.succeed Jul

                8 ->
                    Decode.succeed Aug

                9 ->
                    Decode.succeed Sep

                10 ->
                    Decode.succeed Oct

                11 ->
                    Decode.succeed Nov

                12 ->
                    Decode.succeed Dec

                _ ->
                    Decode.fail <| "Invalid month " ++ String.fromInt x
        )
        Decode.int


zoneDecoder : Decoder ( String, Zone )
zoneDecoder =
    Decode.andThen
        (\timeZoneName ->
            case Dict.get timeZoneName zones of
                Just timeZone ->
                    Decode.succeed <| ( timeZoneName, timeZone () )

                Nothing ->
                    Decode.fail <| "Unknown timeZone " ++ timeZoneName
        )
        Decode.string


init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init { viewport, prefersDarkColorScheme } url _ =
    let
        ( state, cmd ) =
            navigate url Nothing
    in
    ( { state = state
      , viewport = viewport
      , prefersColorScheme =
            if prefersDarkColorScheme then
                Dark

            else
                Light
      }
    , cmd
    )


navigate : Url -> Maybe DisplayData -> ( State, Cmd Msg )
navigate url previousDisplay =
    case url.fragment of
        Just fragment ->
            case percentDecode fragment of
                Just inputString ->
                    case decodeString inputDecoder inputString of
                        Ok input ->
                            let
                                ( timeZoneString, timeZone ) =
                                    input.timeZone

                                date =
                                    partsToPosix timeZone
                                        { year = input.year
                                        , month = input.month
                                        , day = input.day
                                        , hour = input.hour
                                        , minute = input.minute
                                        , second = input.second
                                        , millisecond = 0
                                        }
                            in
                            ( InitializingDisplay
                                { event = input.event
                                , date = date
                                , maybeLocalizedDateString = Nothing
                                , maybeNow = Nothing
                                , previousDisplay = previousDisplay
                                }
                            , requestLocalizedDateString
                                { milliseconds = Time.posixToMillis date
                                , timeZone = timeZoneString
                                , includeSeconds = input.second /= 0
                                }
                            )

                        Err error ->
                            ( InvalidInput <| JsonError error
                            , Cmd.none
                            )

                Nothing ->
                    ( InvalidInput MalformedUrl, Cmd.none )

        Nothing ->
            ( FormPage, Cmd.none )


port requestLocalizedDateString : LocalizedDateStringRequest -> Cmd msg


port receiveLocalizedDateString : (String -> msg) -> Sub msg


port receivePrefersDarkColorScheme : (Bool -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ receiveLocalizedDateString ReceiveLocalizedDateString
        , Browser.Events.onResize ReceiveViewportSize
        , receivePrefersDarkColorScheme ReceivePrefersDarkColorScheme
        , case model.state of
            Display _ ->
                onAnimationFrame Tick

            InitializingDisplay _ ->
                onAnimationFrame Tick

            _ ->
                Sub.none
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ReceiveLocalizedDateString localizedDateString ->
            case model.state of
                InitializingDisplay data ->
                    ( { model
                        | state =
                            attemptTofinishInitialization <|
                                { data
                                    | maybeLocalizedDateString =
                                        Just localizedDateString
                                }
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        ReceiveViewportSize width height ->
            ( { model | viewport = { width = width, height = height } }
            , Cmd.none
            )

        ReceivePrefersDarkColorScheme prefersDarkColorScheme ->
            ( { model
                | prefersColorScheme =
                    if prefersDarkColorScheme then
                        Dark

                    else
                        Light
              }
            , Cmd.none
            )

        Tick posix ->
            case model.state of
                InitializingDisplay data ->
                    ( { model
                        | state =
                            attemptTofinishInitialization <|
                                { data | maybeNow = Just posix }
                      }
                    , Cmd.none
                    )

                Display data ->
                    ( { model | state = Display { data | now = posix } }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        UrlChange url ->
            let
                ( newState, cmd ) =
                    case model.state of
                        Display data ->
                            navigate url <| Just data

                        _ ->
                            navigate url Nothing
            in
            ( { model | state = newState }, cmd )

        NoOp ->
            ( model, Cmd.none )


attemptTofinishInitialization : InitializingDisplayData -> State
attemptTofinishInitialization data =
    let
        { event, date, maybeLocalizedDateString, maybeNow } =
            data
    in
    case ( maybeLocalizedDateString, maybeNow ) of
        ( Just localizedDateString, Just now ) ->
            Display
                { event = event
                , date = date
                , localizedDateString = localizedDateString
                , now = now
                }

        _ ->
            InitializingDisplay data


getColorScheme : ColorSchemePreference -> ColorScheme
getColorScheme preference =
    case preference of
        Light ->
            { background = rgb 1.0 1.0 1.0
            , foreground = rgb 0.1 0.1 0.1
            }

        Dark ->
            { background = rgb 0.1 0.1 0.1
            , foreground = rgb 0.85 0.85 0.85
            }


view : Model -> Browser.Document Msg
view model =
    { title =
        let
            maybeEvent =
                case model.state of
                    InitializingDisplay { event } ->
                        Just event

                    Display { event } ->
                        Just event

                    _ ->
                        Nothing
        in
        case maybeEvent of
            Just event ->
                "Countdown: " ++ event

            Nothing ->
                "Countdown"
    , body = [ layout [] <| ui model ]
    }


ui : Model -> Element Msg
ui model =
    let
        colorScheme =
            getColorScheme model.prefersColorScheme
    in
    el
        [ width fill
        , height fill
        , Background.color colorScheme.background
        , Font.color colorScheme.foreground
        , Font.family [ Font.sansSerif ]
        ]
    <|
        el [ centerX, centerY, padding 16 ] <|
            case model.state of
                FormPage ->
                    text "FormPage"

                InitializingDisplay { previousDisplay } ->
                    case previousDisplay of
                        Just data ->
                            displayUi colorScheme model.viewport data

                        Nothing ->
                            none

                Display data ->
                    displayUi colorScheme model.viewport data

                InvalidInput error ->
                    case error of
                        MalformedUrl ->
                            text "Malformed URL"

                        JsonError jsonError ->
                            html <|
                                Html.pre []
                                    [ Html.text <|
                                        Decode.errorToString jsonError
                                    ]


displayUi : ColorScheme -> Viewport -> DisplayData -> Element Msg
displayUi colorScheme viewport { event, date, localizedDateString, now } =
    column [ spacing 32 ]
        [ column [ centerX, spacing 8 ]
            [ paragraph [ Font.center, Font.size 32 ] [ text event ]
            , paragraph [ Font.center ] [ text localizedDateString ]
            ]
        , row
            [ centerX
            , Font.size (min 72 (viewport.width // 7))
            , spacing (min 16 (viewport.width // 32))
            ]
          <|
            if Time.posixToMillis now < Time.posixToMillis date then
                let
                    { days, hours, minutes, seconds } =
                        chunkTimespan <|
                            Time.posixToMillis date
                                - Time.posixToMillis now
                in
                [ text <| String.fromInt days
                , el
                    [ height fill
                    , width (px (min 4 (viewport.width // 128)))
                    , Background.color colorScheme.foreground
                    ]
                    none
                , text <|
                    String.padLeft 2 '0' (String.fromInt hours)
                        ++ ":"
                        ++ String.padLeft 2 '0' (String.fromInt minutes)
                        ++ ":"
                        ++ String.padLeft 2 '0' (String.fromInt seconds)
                ]

            else
                [ text "\u{1F973}" ]
        ]


chunkTimespan : Int -> ChunkedTimespan
chunkTimespan total =
    let
        totalSeconds =
            ceiling <| toFloat total / 1000

        ( days, daysRemainder ) =
            divisionWithRemainderBy secondsPerDay totalSeconds

        ( hours, hoursRemainder ) =
            divisionWithRemainderBy secondsPerHour daysRemainder

        ( minutes, seconds ) =
            divisionWithRemainderBy secondsPerMinute hoursRemainder
    in
    { days = days
    , hours = hours
    , minutes = minutes
    , seconds = seconds
    }


secondsPerMinute : Int
secondsPerMinute =
    60


secondsPerHour : Int
secondsPerHour =
    secondsPerMinute * 60


secondsPerDay : Int
secondsPerDay =
    secondsPerHour * 24


divisionWithRemainderBy : Int -> Int -> ( Int, Int )
divisionWithRemainderBy divisor dividend =
    ( floor <| toFloat dividend / toFloat divisor
      -- // overflows with large numbers
    , remainderBy divisor dividend
    )
