let
  pkgs = import ./nixpkgs.nix {};

  mkDerivation =
    { srcs ? ./elm-srcs.nix
    , src
    , name
    , srcdir ? "./src"
    , targets ? []
    , registryDat ? ./registry.dat
    }:
    pkgs.stdenv.mkDerivation {
      inherit name src;

      buildInputs = with pkgs; [ elmPackages.elm nodePackages.uglify-js ];

      buildPhase = pkgs.elmPackages.fetchElmDeps {
        elmPackages = import srcs;
        inherit registryDat;
        elmVersion = "0.19.1";
      } + "make build\n";

      installPhase = ''
        cp -r build $out
      '';
    };
in mkDerivation {
  name = "elm-app-0.1.0";
  srcs = ./elm-srcs.nix;
  src = ./.;
  targets = ["Main"];
  srcdir = "./src";
}

