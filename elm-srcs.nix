{

      "mdgriffith/elm-ui" = {
        sha256 = "17iayf13r5fy38xlliaizi6af12jp1yw2vjxyhi6qf16xy579vn8";
        version = "1.1.5";
      };

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "elm/browser" = {
        sha256 = "0nagb9ajacxbbg985r4k9h0jadqpp0gp84nm94kcgbr5sf8i9x13";
        version = "1.0.2";
      };

      "justinmimbs/time-extra" = {
        sha256 = "0s5r0w5dfyrwpkgx594p3z39zrvz2gnxfzb6qlddwfibiaj2nfz9";
        version = "1.1.0";
      };

      "elm/core" = {
        sha256 = "19w0iisdd66ywjayyga4kv2p1v9rxzqjaxhckp8ni6n8i0fb2dvf";
        version = "1.0.5";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "justinmimbs/timezone-data" = {
        sha256 = "0i4x7xjcw7q8alp68mwldxbmzp0mp2vb8xc26j5yi5qbsdq92wq3";
        version = "2.1.4";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "NoRedInk/elm-json-decode-pipeline" = {
        sha256 = "0y25xn0yx1q2xlg1yx1i0hg4xq1yxx6yfa99g272z8162si75hnl";
        version = "1.0.0";
      };

      "justinmimbs/date" = {
        sha256 = "0gl31dsbzryc29749wwi4jfnidsyicbplawmc8ar1vbxaa39ld0v";
        version = "3.2.0";
      };

      "elm/parser" = {
        sha256 = "0a3cxrvbm7mwg9ykynhp7vjid58zsw03r63qxipxp3z09qks7512";
        version = "1.1.0";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };
}
